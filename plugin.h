#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Utility/ipomodorosettingsdataextention.h"
#include "pomodorosettingsdataextention.h"
#include "settings.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.PomodoroSettingsDataExtention" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	~Plugin() override;

	// PluginBase interface
protected:
	void onReady() override;

private:
	DataExtention* m_dataExtention;
	Settings* m_settings;
};
